const axios = require('axios')

const VLM_API_ACCESS_ID = '***'
const VLM_API_ACCESS_KEY = '***'

const VLM_API_SESSION_URL = 'https://api.voluum.com/auth/access/session'
const VLM_API_CREATE_CAMP_URL = 'https://api.voluum.com/campaign'

const campPostBody = {
  costModel: {
    type: "AUTO"
  },
  namePostfix: "[QA] TEST2 28.12.20",
  redirectTarget: {
    inlineFlow: {
      active: true,
      defaultOfferRedirectMode: "DOUBLE_HTML",
      defaultPaths: [
        {
          active: true,
          autoOptimized: false,
          calculationMethod: "ROI",
          listicle: false,
          realtimeRoutingApiState: "DISABLED",
          landers: [
            {
              lander: {
                id: "b082b3e9-6657-4b76-976e-63a3aad1065c"
              },
              weight: 10
            }
          ],
          landersDisplaySortOrder: "ASC",
          name: "TEST",
          offerRedirectMode: "DOUBLE_HTML",
          offers: [
            {
              offer: {
                id: "d6943cb5-6b75-40d7-82c3-3a3db0829f53"
              },
              weight: 10
            },
            {
              offer: {
                id: "56045485-8eb3-42ce-9b01-6fd7d4db9cf0"
              },
              weight: 10
            },
            {
              offer: {
                id: "a1281fa9-3575-4a81-8de7-7eec8fa50abd"
              },
              weight: 10
            }
          ],
          offersDisplaySortOrder: "ASC",
          weight: 10
        }
      ],
      name: "TEST"
    }
  },
  workspace: {
    id: "53f1b614-ca76-46cb-ad3d-761875ab471d"
  },
  trafficSource: {
    id: "35f8ab4e-3248-4f86-ab47-708a3d29b1fe"
  }
}

!(async () => {
  try {
    // var {data: { token }} = await axios.post(VLM_API_SESSION_URL, {
    //   accessId: VLM_API_ACCESS_ID,
    //   accessKey: VLM_API_ACCESS_KEY
    // })

    const token = "DMebTZwiXaATbyGlAbeJoiP7SdytaCfV"

    const vlmHeaders = {
      "cwauth-token": token
    }
  
    var resp = await axios.post(VLM_API_CREATE_CAMP_URL, campPostBody, { headers: vlmHeaders })
    console.log(resp.data)
  } catch(err) {
    console.log(err.response)
    err.response && err.response.data && err.response.data.error && err.response.data.error.messages && console.log(err.response.data.error.messages)
  }
})()